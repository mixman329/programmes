! pour la compilation, utilisez : gfortran hermite.f90 testhermite.f90 -o testhermite

program test
    real*8 :: psi(1024,0:5),x(1024),hermite
    integer :: i,n
    do n=0,5
        do i=1,1024
            x(i)=-5.d0+(i-1)*0.01d0
            psi(i,n)=hermite(n,x(i))
        end do
    end do
    do i=1,1024
        write(97,'(7E23.6)') x(i),(psi(i,n),n=0,5)
    end do

end program test
