
real(8) function hermite(n,xo,a,x,npts)
! (sqrt(a))*(x(j)-xo)=ksi!
implicit none
! arguments
integer, intent(in) :: n,npts
real(8), intent(in) :: x,xo,a
! local variables
integer::i
real(8) h1,h2,ht

if (n.eq.0) then
  hermite=1.d0
  return
end if
if (n.eq.1) then
  hermite=2.d0*(sqrt(a))*(x-xo)
  return
end if
h1=2.d0*(sqrt(a))*(x-xo)
h2=1.d0
do i=2,n
  ht=2.d0*(sqrt(a))*(x-xo)*h1-2.d0*dble(i-1)*h2
  h2=h1
  h1=ht
end do
hermite=h1
return
end function
!EOC

