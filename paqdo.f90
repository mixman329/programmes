program paqdo
use modhermite
implicit none
interface
	real(8) function hermite(n,xo,a,x,npts)
		integer, intent(in) :: n,npts
		real(8), intent(in) :: xo,a
		real*8, dimension (1:npts):: x
		! local variables
		integer i
		real(8) h1,h2,ht
	end function hermite
end interface
!déclaration de variables
real*8::xmax,xmin,xo,dx,m,w,a,aire
integer::j,npts,n,nmax,tf,t
real*8, dimension (:), allocatable:: x,norm,en
real*8, dimension (:,:), allocatable::phi,her
complex*16::i
complex*16, dimension (:,:), allocatable::psi
 character(1)::ok
 character(9)::fname,potnm
!initialisation de variables
xmax=5.d0
xmin=-5.d0
xo=0.d0
npts=1024
dx=(xmax-xmin)/dfloat(npts-1)
m=918.d0 
w=0.104d0 
a=m*w
n=0
i=(0.d0,1.d0)

!lecture des paramètres
do
	write (*,*) 'entrez les entiers nmax et tf'
	read (*,*) nmax
	read (*,*) tf
	write (*,'(A5,I2.1,A4,I3.1)')  'nmax=',nmax,' tf=',tf
	write (*,'(A40)') 'ces paramètres sont-ils corrects? [y/n]'
	read (*,*) ok
	if (ok == 'y') exit
	write (*,*) ok,', essayez de nouveau'
end do

!allocation des vecteurs
allocate(her(1:npts,0:nmax))
allocate(norm(0:nmax))
allocate(x(1:npts))
allocate(phi(1:npts,0:nmax))
allocate(psi(1:npts,tf))
allocate(en(0:nmax))

!calcul de phi

do n=0,nmax
	do j=1,npts
		call subhermite (her,x,xmin,dx,hermite,j,n,nmax,a,xo,npts)!
		phi(j,n)=her(j,n)*exp(-a/2.d0*(x(j)-xo)**2.d0)
		
	end do
end do

!calcul de la normalisation
do n=0,nmax
	aire=0
	do j=1,npts-1
		aire=aire+abs(phi(j+1,n)+phi(j,n))*dx/2.d0
	end do
	norm(n)=1.d0/aire
	write (*,'(A37,I2.1,A5)') 'la constante de normalisation pour n=',n,' est:'
	write (*,*) norm(n)
end do

!calcul de phi normalisé
do n=0,nmax
	write(fname,'(A3,I2.2,A4)') 'phi',n,'.dat'
	open (13,file=fname,status='unknown')
	do j=1,npts
		phi(j,n)=phi(j,n)*norm(n)
		write (13,*) x(j),phi(j,n)
	end do
	close(13)
end do

!calcul de l'énergie
do n=0,nmax
	en(n)=(n+0.5d0)*w
	write(*,'(A20,I2.1,A5)')'L''énergie du niveau',n,' est:'
	write(*,*) en(n)
end do

!potentiel harmonique et ses états stationnaires
do n=0,nmax
	write (potnm,'(A3,I2.2,A4)')'pot',n,'.dat'
	open(15,file=potnm,status='unknown')
	do j=1,npts
		phi(j,n)=phi(j,n)+en(n)
		write (15,*) x(j),phi(j,n)
	end do
	close(15)
end do

end program paqdo
